# lpc5500_cookbook

#### 介绍

nxp lpc5500系列微控制器, 使用lpcxpresso55s69 evk开发板.

之前的样例代码库使用的是Fro96MHz的系统主频, 本代码仓库提取了新版MCUX SDK v2.7.0中的源代码, 默认使用PLL0输出的150MHz作为内核的时钟源, 重构了bsp代码.

之前部分有价值的工程也会在本代码仓库中重建.

#### 软件架构

各项目的工程文件存放在"\application"目录下:

- hello_world: 使用printf通过串口打印格式化字符串, 模板工程.
- lcd_basic: 使用微雪科技的2.8寸Arduino接口的LCD模块(hx8347d控制器), 实现刷屏的基本工程.
- sdcard_basic: 使用fatfs文件系统访问SD卡的基本工程. 基于轮询的高速spi接口, 可以实现30fps以上的刷屏速度.
- jpeg_lcd_sdcard: 使用东洋小哥哥(fatfs的作者)实现的tjpgd1c解码器实现jpeg解码的样例工程. 本工程是以消耗内存最小的方式实现移植, 边解码边显示, 刷屏速度比较慢, 但能直观地看到解码的速度.
- jpeg_lcd_sdcard_v0.2: 同上, 但使用了显存的技术, 开辟一块内存存放解码后的图像数据, 解码完成后才一起刷屏, 显示效果好.
- dualcore_basic: 启用双核开发的模板工程, 其中包含一个基于共享内存的双核通信小组件shmem.
- powerquad_kws_tcn_lcd: 使用PowerQuad实现的TCN神经网络, 语音关键字识别. 本例中实现了识别0-9个数字, 并做了一个匹配语音验证码的demo. 本例使用LCD和串口同时作为匹配结果的输出. 没有LCD模块亦可运行, 对LCD模块的控制放在副核core1上, 是否刷屏不影响主核core0的计算性能.
- powerquad_fft1024_benchmark: 使用PowerQuad实现1024点FFT的样例工程, 并同cmsis-dsp中的cpu实现方式进行比较.
- powerquad_dct2d_benchmark: 使用PowerQuad实现2维DCT变换的样例工程, 并同cmsis-dsp中的cpu实现方式进行比较.
- powerquad_dct2d_jpeg_lcd: 使用PowerQaud实现的2维DCT算法替换掉tjpgd1c中的8x8数据块解析的部分, 成功实现解码jpeg图片的功能. (实际效果还需要微调).
- bootloader_ram_application: 启动后将代码从flash复制到ram中运行, 提高application的运行速度.
- coremark_basic: 移植coremark到lpc5500平台上, 对代码进行微调, 使其能正常运行.
- coremark_ram_bootloader: 利用bootloader_ram_application工程中使用的技术, 将coremark程序搬运到sram中运行, 同在flash中运行的coremark_basic工程对比, 进行性能分析. flash工程在-o3优化下能跑250左右, ram工程能跑到400左右.
- nn_fc_self_training: 在lpc5500 mcu上实现的一个能够自己训练自己的全连接人工神经网络. 除了实现功能外, 本工程用于对mcu端实现自学习功能提供一些benchmark的评估数据, 例如计算时间, 内存占用等等. 将为进一步应用奠定基础.
- cjson_basic: 在MCU上使用cJSON组件解析包含JSON内容的字符串

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
