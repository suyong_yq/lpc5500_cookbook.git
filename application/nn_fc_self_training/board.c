/*
 * Copyright 2017-2018 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"

#include "fsl_clock.h"
#include "fsl_iocon.h"
#include "fsl_reset.h"

#include "fsl_gpio.h"
#include "fsl_usart.h"

#include <stdio.h>

/*******************************************************************************
 * Variables
 ******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/
/* Initialize debug console. */
void BOARD_InitDebugConsole(void)
{
    /* attach 12 MHz clock to FLEXCOMM0 (debug console) */
    CLOCK_AttachClk(kFRO12M_to_FLEXCOMM0);

    usart_config_t usart_config;
    usart_config.baudRate_Bps = 115200u;
    usart_config.parityMode = kUSART_ParityDisabled;
    usart_config.stopBitCount = kUSART_OneStopBit;
    usart_config.bitCountPerChar = kUSART_8BitsPerChar;
    usart_config.loopback = false;
    usart_config.enableTx = true;
    usart_config.enableRx = true;
    USART_Init(USART0, &usart_config, CLOCK_GetFreq(kCLOCK_Fro12M));
}

void board_init(void)
{
    BOARD_InitPins();
    BOARD_BootClockPLL150M();
    BOARD_InitDebugConsole();
}

int fputc(int ch, FILE *f)
{
	uint8_t tmp8 = (uint8_t)ch;

    USART_WriteBlocking(USART0, &tmp8, 1u);

 	return ch;
}

int fgetc(FILE *f)
{
	uint8_t tmp8;

	USART_ReadBlocking(USART0, &tmp8, 1u);

	return tmp8;
}

/* EOF. */

