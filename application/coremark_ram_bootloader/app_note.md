# 调试笔记

## 概述

实现在RAM中运行应用程序的功能. 不同于在ram中调试, 硬件复位后程序将被清空. 本工程将实际运行的代码存放在片内flash中, 通过专门设计的启动程序, 将应用代码从flash复制到ram中, 然后启动ram中的完整应用程序. 这样每次复位后都可以重新运行应用程序, 同时还利用了在ram中运行程序速度快的优势.

实现的思路来自于双核调试. 只是在本工程使用仅使用一个核而已. 本工程的设计方法也适用于其它单核应用程序.

lpc5500的ram分配:

- 32KB的sramx在地址上是孤立的, 并且只能存放程序, 不能存放数据.
- sram0, sram1, sram2, sram3各64KB.
- 16KB的sram4是给powerquad专用的, 也不考虑
- 16KB的usbram是给usb专用的, 也不考虑

最终设计demo实现在同一片lpc5500上保存两个应用程序:
1. 使用sram, sram1, sram2存放运行时程序, 使用sram3存放运行时数据
2. 使用sramx存放运行时程序, 使用sram3存放运行时数据.

由于在某个启动过程中只有一个程序处于正在运行的状态, 因此可以复用sram3的内存空间.

哎, 算了, 不要搞双固件了, 先用单固件做demo. 双固件的设计同但固件相似, 但需要解决双固件并存的问题, 这种具体问题只有在需要的时候才有必要考虑.

关于flash的使用:
- bootloader部分占用开始的64KB, 考虑到后续功能的扩展, 例如实现基于sdcard的boot等.
- 应用程序实际只占用192KB的空间, 那就将紧接着boot之后的64KB - 256KB.

## 设计步骤

1. application工程

主要是搞linker

2. bootloader工程

主要是搞跳转


## 实验步骤

首次下载时, 先编译application工程, 然后再编译bootloader工程, 此时bootloader工程中会包含application工程的代码.
下载, 然后正常运行.

后续调试application工程, 通过jlink或者其它调试器, 可以直接将程序下载到ram中, 由于bootloader程序已经在flash中, 所以使用ide的热复位是可以回到application工程中的. 此时完全不影响在线调试, 同之前直接调试flash中的程序完全一样.

当完成application工程的调试, 需要将程序固化在flash中时, 重新编译bootloader工程, 包含最新的application程序, 一次性下载到flash中. 之后哪怕是POR也能启动到application中了.