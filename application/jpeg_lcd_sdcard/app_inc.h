/* app_inc.h */
#ifndef __APP_INC_H__
#define __APP_INC_H__

#include <stdio.h>
#include "fsl_common.h"

#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"

#include "fsl_gpio.h"

#include "ff.h"
#include "lcd_hx8347d.h"
#include "lcd_fonts.h"
#include "tjpgd.h"

extern uint8_t app_jpg_pixel_buff[LCD_WIDTH*LCD_HEIGHT*2]; /* RGB565. */

FRESULT fs_init(void);
FRESULT fs_load_jpg(char *filename, uint8_t scale);
//char * fs_dir_get_father_path(char *fullpath);
//char * fs_dir_get_filename(char *fullpath);

void touch_init(void);
void touch_wait_blocking(void);

#endif /* __APP_INC_H__ */

