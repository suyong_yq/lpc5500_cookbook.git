/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "app_inc.h"

#include "fsl_clock.h"
#include "fsl_gpio.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TOUCH_IRQ_PORT_IDX    1u //1u  //0u //1u
#define TOUCH_IRQ_PIN_IDX     6u //18u //5u //6u

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/
void touch_init(void)
{
    CLOCK_EnableClock(kCLOCK_Iocon);

    const uint32_t iocon_pin_config = (/* Pin is configured as FC0_RXD_SDA_MOSI_DATA */
                                         IOCON_PIO_FUNC0 |
                                         /* No addition pin function */
                                         IOCON_PIO_MODE_INACT |
                                         /* Standard mode, output slew rate control is enabled */
                                         IOCON_PIO_SLEW_STANDARD |
                                         /* Input function is not inverted */
                                         IOCON_PIO_INV_DI |
                                         /* Enables digital function */
                                         IOCON_PIO_DIGITAL_EN |
                                         /* Open drain is disabled */
                                         IOCON_PIO_OPENDRAIN_DI);
    IOCON_PinMuxSet(IOCON, TOUCH_IRQ_PORT_IDX, TOUCH_IRQ_PIN_IDX, iocon_pin_config);

    CLOCK_EnableClock(kCLOCK_Gpio1);

    gpio_pin_config_t gpio_pin_config;
    gpio_pin_config.pinDirection = kGPIO_DigitalInput;
    GPIO_PinInit(GPIO, TOUCH_IRQ_PORT_IDX, TOUCH_IRQ_PIN_IDX, &gpio_pin_config);
}

/* PIO1_4. */

void touch_wait_blocking(void)
{
    while (1u == GPIO_PinRead(GPIO, TOUCH_IRQ_PORT_IDX, TOUCH_IRQ_PIN_IDX))
    {}
}

/* EOF. */

