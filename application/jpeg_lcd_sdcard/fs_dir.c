/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "app_inc.h"

#include "ff.h"
#include "string.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/


char fs_dir_path_buff[64]; /* common buff for dir path. */
char fs_filename_buff[64]; /* common buff for dir path. */

/* 删掉最后一个路径分隔符及其之后的内容. */
char * fs_dir_get_father_path(char *fullpath)
{
    char * path_ptr;

    strcpy(fs_dir_path_buff, fullpath);
    path_ptr = strrchr(fs_dir_path_buff, '/');
    if (path_ptr == NULL)
    {
        return NULL;
    }

    *path_ptr = '\0'; /* cut off the following charactors. */

    return fs_dir_path_buff;
}

/* 提取最后一个分隔符后面的文件名. */
char * fs_dir_get_filename(char *fullpath)
{
    char * path_ptr;

    path_ptr = strrchr(fullpath, '/');

    path_ptr++;
    strcpy(fs_filename_buff, path_ptr);

    return fs_filename_buff;
}

/* EOF. */

