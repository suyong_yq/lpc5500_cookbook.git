
Hi, all

to let you know the result as soon as possible, i prefer not to write an formal report for this test case now. i would privide a brief description and the source code, so that you can check the code when you need. well, the final report would be done later once i am available.

totally 5 kinds of way to computing the fft with 1024 in my test project.
- fft1024_arm_func1() is using the cmsis-dsp api to calc the fft 1024 directly.
- fft1024_arm_func2() is using method of spliting a fft 1024 into two fft 512, and then assemble the result. the fft function is also from cmsis-dsp. this function is also to verify if the method is right.
- fft1024_powerquad_func1() is using the method, but replace the implementation of fft 512 part with powerquad.
- fft1024_powerquad_func2() is using the method, but replace the implementation of fft 512 and the vector addition part with powerquad.
- fft1024_powerqaud_func3() is using the method, but replace the implementation of fft 512 , vector addition, and the vector complex multiplication (with steps) with powerquad.

IAR IDE with different optimization level, LPCXpresso55s69 EVK, 150MHz.

finally, a table is created to summaize the timing record. the unit for the timing number is "us".

Table:

