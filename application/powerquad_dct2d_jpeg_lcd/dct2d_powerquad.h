/* dct2d_powerquad.h */
#ifndef __DCT2D_POWERQUAD_H__
#define __DCT2D_POWERQUAD_H__

#include "fsl_common.h"
#include "arm_math.h"

#define DCT2D_MATRIX_N  8u

void dct2d_powerquad_init(void);
void dct2d_powerquad_dct_f32(float32_t *input_matrix, float32_t *output_matrix);
void dct2d_powerquad_idct_f32(float32_t *input_matrix, float32_t *output_matrix);

void print_matrix_f32(float *matrix, uint32_t row_num, uint32_t col_num);

#endif /* __DCT2D_POWERQUAD_H__ */

