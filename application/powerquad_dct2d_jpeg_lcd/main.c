/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "app_inc.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
FRESULT app_fs_err;
char app_fs_filename[64];

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
    board_init();
    dct2d_powerquad_init();

    printf("\r\nJPGE Decompressor example.\r\n");

    lcd_init();
    lcd_clear_screen(LCD_COLOR_WHITE);

    touch_init();

    /* setup the file system. */
    app_fs_err = fs_init();
    if (app_fs_err != FR_OK)
    {
        printf("fs_init() failed.\r\n");
        while (1);
    }
    printf("fs_init() done.\r\n");


    while (1)
    {
        for (uint32_t i = 0u; i < 19u; i++)
        {
            sprintf(app_fs_filename, "/jpg/jpg_240x320_%d.jpg", i);
            app_fs_err = fs_load_jpg(app_fs_filename, 0);
            if (app_fs_err != FR_OK)
            {
                printf("fs_load_jpg() failed.\r\n");
            }
            else
            {
                printf("fs_load_jpg() done.\r\n\r\n");
                lcd_print_string_1608(0u, app_fs_filename, LCD_COLOR_RED);
            }

            //getchar(); /* next. */
            touch_wait_blocking();
        }

        for (uint32_t i = 1u; i <= 4u; i++)
        {
            sprintf(app_fs_filename, "/jpg/jpg_480x640_%d.jpg", i);
            app_fs_err = fs_load_jpg(app_fs_filename, 1);
            if (app_fs_err != FR_OK)
            {
                printf("fs_load_jpg() failed.\r\n");
            }
            else
            {
                printf("fs_load_jpg() done.\r\n\r\n");
                lcd_print_string_1608(0u, app_fs_filename, LCD_COLOR_RED);
            }

            //getchar(); /* next. */
            touch_wait_blocking();
        }

        for (uint32_t i = 1u; i <= 3u; i++)
        {
            sprintf(app_fs_filename, "/jpg/jpg_960x1280_%d.jpg", i);
            app_fs_err = fs_load_jpg(app_fs_filename, 2);
            if (app_fs_err != FR_OK)
            {
                printf("fs_load_jpg() failed.\r\n");
            }
            else
            {
                printf("fs_load_jpg() done.\r\n\r\n");
                lcd_print_string_1608(0u, app_fs_filename, LCD_COLOR_RED);
            }

            //getchar(); /* next. */
            touch_wait_blocking();
        }

    }
}

/* EOF. */

