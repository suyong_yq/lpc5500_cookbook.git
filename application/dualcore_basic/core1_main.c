/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "core1_app_inc.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
uint32_t                           app_fifo1_data_raw;
shmem_fifo_cmd_msg_t *app_cmd_msg_ptr;
uint32_t                           app_cmd_code;
uint32_t                           app_cmd_param[SHMEM_FIFO_PARAM_NUM];

/*******************************************************************************
 * Code
 ******************************************************************************/
/*
void my_delay(uint32_t t)
{
    for (uint32_t i = 0u; i < t; i++)
    {
        for (uint32_t j = 0u; j < 100000; j++)
        {
            asm("nop");
        }
    }
}
*/

/*!
 * @brief Main function
 */
int main(void)
{
    shmem_set_core_events(1u, SHMEM_CORE1_STAT_MAIN_START_MASK); /* boot ready. */

    led_init();
    shmem_set_core_events(1u, SHMEM_CORE1_STAT_HW_INIT_DONE_MASK); /* hardware ready. */

    shmem_fifo_init(1u);
    shmem_set_core_events(1u, SHMEM_CORE1_STAT_FIFO1_READY_MASK); /* fifo ready. */

    while (1)
    {
        app_fifo1_data_raw = shmem_fifo_pop(1u);
        if (app_fifo1_data_raw == 0u)
        {
            continue;
        }

        app_cmd_msg_ptr = (shmem_fifo_cmd_msg_t *)app_fifo1_data_raw;
        app_cmd_code = app_cmd_msg_ptr->cmd;
        for (uint32_t i = 0u; i < SHMEM_FIFO_PARAM_NUM; i++)
        {
            app_cmd_param[i] = app_cmd_msg_ptr->param[i];
        }
        app_cmd_msg_ptr->is_new = false; /*  release the message.*/

        switch (app_cmd_code)
        {
        case shmem_fifo1_cmd_led_ctrl:
            if (app_cmd_param[0] == 0u)
            {
                led_off(1u << 0u);
            }
            else
            {
                led_on(1u << 0u);
            }
            break;
        default:
            break;
        }

        //led_on(1u<< 0u);
        //my_delay(10);
        //led_off(1u << 0u);
        //my_delay(10);
    }
}
/* EOF. */

