/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>

#include "fsl_common.h"
#include "board.h"
#include "shmem.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
void core1_init(void);

/*******************************************************************************
 * Variables
 ******************************************************************************/
shmem_fifo_cmd_msg_t app_fifo1_cmd_msg;

/*******************************************************************************
 * Code
 ******************************************************************************/
void my_delay(uint32_t t)
{
    for (uint32_t i = 0u; i < t; i++)
    {
        for (uint32_t j = 0u; j < 100000; j++)
        {
            asm("nop");
        }
    }
}

/*!
 * @brief Main function.
 */
int main(void)
{
    board_init();
    printf("lpc5500 durecore example\r\n");

    core1_init();
    shmem_wait_core_events(1u, SHMEM_CORE1_STAT_MAIN_START_MASK);
    printf("core1 boot ready.\r\n");

    shmem_wait_core_events(1u, SHMEM_CORE1_STAT_HW_INIT_DONE_MASK);
    printf("core1 hardware ready.\r\n");

    shmem_wait_core_events(1u, SHMEM_CORE1_STAT_FIFO1_READY_MASK);
    printf("core1 fifo ready.\r\n");

    while (1)
    {
        /* led on. */
        app_fifo1_cmd_msg.cmd = shmem_fifo1_cmd_led_ctrl;
        app_fifo1_cmd_msg.param[0] = 1u; /* led on. */
        shmem_fifo_push(1u, (uint32_t)(&app_fifo1_cmd_msg));
        my_delay(100);

        /* led off. */
        app_fifo1_cmd_msg.cmd = shmem_fifo1_cmd_led_ctrl;
        app_fifo1_cmd_msg.param[0] = 0u; /* led off. */
        shmem_fifo_push(1u, (uint32_t)(&app_fifo1_cmd_msg));
        my_delay(100);
    }
}

/* EOF. */

