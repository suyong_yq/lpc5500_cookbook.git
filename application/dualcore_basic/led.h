/* led.h */
#ifndef __LED_H__
#define __LED_H__

#include <stdint.h>

#define LED_NUM  1u

void led_init(void);
void led_on(uint32_t mask);
void led_off(uint32_t mask);

#endif /* __LED_H__ */

