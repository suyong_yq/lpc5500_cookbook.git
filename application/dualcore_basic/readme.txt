

双核系统实现主从核协同工作架构:
- 主核统管iocon和根clock source. 也就是说, core0和core1共用pin_mux和clock_config.
- 从核对所属硬件资源完全占有是在IP层面上的. 例如spi, gpio等模块, 甚至core和core1都会独享nvic. 另外, core0和core1有各自对外设模块访问开关的控制, 两个core对同一个外设的访问总线通路也是各自不同的, 这就能够解释当core1开启了usart5的中断后, usart5发出的中断不会被core0捕获到.




