﻿/* core0_boot_core1.c */
#include "app_inc.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define CORE1_BOOT_ADDRESS (void *)0x04000000

/*******************************************************************************
 * Variables
 ******************************************************************************/
extern uint8_t core1_image_start[]; /* defined in linker file. */

/*******************************************************************************
 * Code
 ******************************************************************************/
void start_core1_hardware(void * boot_addr)
{
    SYSCON->CPUCFG  |= SYSCON_CPUCFG_CPU1ENABLE_MASK; /* enable core1. */
    SYSCON->CPBOOT = SYSCON_CPBOOT_CPBOOT(boot_addr);/* provide boot addr to core1. */
    /* reset the core1. */
    SYSCON->CPUCTRL = 0xc0c48000 /* verification code. */
                                      | SYSCON_CPUCTRL_CPU1CLKEN_MASK | SYSCON_CPUCTRL_CPU1RSTEN_MASK; /* fall into reset. */
    SYSCON->CPUCTRL = 0xc0c48000 /* verification code. */
                                      | SYSCON_CPUCTRL_CPU1CLKEN_MASK; /* leave reset. */
}

void core1_init(void)
{
    /* copy_core1_image_to_sramx. */
#pragma section = "__sec_core1" /* 声明一个section, 具体这个section在project option中定义 */
    uint32_t core1_image_size = (uint32_t)__section_end("__sec_core1") - (uint32_t)&core1_image_start;

    printf("core1_image_start: 0x%x\r\n", (uint32_t)&core1_image_start);
    printf("core1_image_size : %d Byte,  %d KB\r\n", core1_image_size, core1_image_size/1024);

    /* Copy Secondary core application from FLASH to the target memory. */
    memcpy(CORE1_BOOT_ADDRESS, (void *)core1_image_start, core1_image_size);

    start_core1_hardware(CORE1_BOOT_ADDRESS);
}

/* EOF. */

