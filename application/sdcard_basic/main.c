/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "app_inc.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
FRESULT app_fs_err;
DIR     app_fs_dir; /* Directory object */
FILINFO app_fs_fileinfo;

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
	//uint8_t ch;
    int err;

    BOARD_InitPins();
    BOARD_BootClockPLL150M();
    BOARD_InitDebugConsole();

    printf("\r\nsdcard basic example.\r\n");

    //lcd_init();
    //lcd_clear_screen(LCD_COLOR_WHITE);

    err = ff_init();
    if (err < 0)
    {
        printf("ff_init() failed.\r\n");
        while (1);
    }
    printf("ff_init() done.\r\n");

    /* list dirs and files. */
    printf("list files in root directory:\r\n");
    //app_fs_err = f_opendir(&app_fs_dir, "/bmp");
    app_fs_err = f_opendir(&app_fs_dir, "/");
    if (FR_OK != app_fs_err)
    {
        printf("f_opendir() failed.\r\n");
        while (1);
    }
    for (;;)
    {
        app_fs_err = f_readdir(&app_fs_dir, &app_fs_fileinfo);

        /* To the end. */
        if ((app_fs_err != FR_OK) || (app_fs_fileinfo.fname[0U] == 0U))
        {
            break;
        }
#if 0
        if (app_fs_fileinfo.fname[0] == '.')
        {
            continue;
        }
#endif
        if (app_fs_fileinfo.fattrib & AM_DIR)
        {
            printf("directory: %s\r\n", app_fs_fileinfo.fname);
        }
        else
        {
            printf("file: %s\r\n", app_fs_fileinfo.fname);
        }
    }

    printf("\r\nexample done.\r\n");

    while (1)
    {
    	//ch = getchar();
    	//putchar(ch);
        //lcd_clear_screen(LCD_COLOR_BLUE);

        //ch = GETCHAR();
    	//PUTCHAR(ch);
        //lcd_clear_screen(LCD_COLOR_RED);
    }
}

/* EOF. */

