/* TCN_param.h */

#ifndef __TCN_PARAM_H__
#define __TCN_PARAM_H__

extern float  Wpre [ ];
extern float  W0 [ ];
extern float  W1 [ ];
extern float  W2 [ ];
extern float  W3 [ ];
extern float  W4 [ ];
extern float  Bpre [ ];
extern float  B0 [ ];
extern float  B1 [ ];
extern float  B2 [ ];
extern float  B3 [ ];
extern float  B4 [ ];

#endif /* __TCN_PARAM_H__ */

