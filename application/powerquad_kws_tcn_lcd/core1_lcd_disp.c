/*
 * Copyright 2017-2018 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>

#include "fsl_common.h"
#include "lcd_hx8347d.h"
#include "lcd_fonts.h"

/*******************************************************************************
 * Variables
 ******************************************************************************/
char app_lcd_print_buff[64];

/*******************************************************************************
 * Code
 ******************************************************************************/


#define APP_LCD_WELCOME_TITLE_BASE_LINE   4u
#define APP_LCD_WELCOME_README_BASE_LINE  12u

void app_lcd_welcome(void)
{
    lcd_clear_screen(LCD_COLOR_BLACK);

    /* title. */
    sprintf(app_lcd_print_buff, "    LPC5500");
    lcd_print_string_3216(1u, app_lcd_print_buff, LCD_COLOR_RED);

    sprintf(app_lcd_print_buff, "NXP");
    lcd_print_string_3216(1u, app_lcd_print_buff, LCD_COLOR_YELLOW);

    sprintf(app_lcd_print_buff, "   PowerQuad\r\n");
    lcd_print_string_3216(2u, app_lcd_print_buff, LCD_COLOR_RED);

    sprintf(app_lcd_print_buff, "with");
    lcd_print_string_1608(5u, app_lcd_print_buff, LCD_COLOR_BLUE);

    sprintf(app_lcd_print_buff, "       demo");
    lcd_print_string_1608(7u, app_lcd_print_buff, LCD_COLOR_BLUE);

    sprintf(app_lcd_print_buff, "KWS");
    lcd_print_string_3216(3u, app_lcd_print_buff, LCD_COLOR_RED);

    /* readme. */
    sprintf(app_lcd_print_buff, "Line-In -> MCU");
    lcd_print_string_1608(APP_LCD_WELCOME_README_BASE_LINE,
                          app_lcd_print_buff, LCD_COLOR_GREEN);

    sprintf(app_lcd_print_buff, "           MCU -> Earphone");
    lcd_print_string_1608(APP_LCD_WELCOME_README_BASE_LINE+1,
                          app_lcd_print_buff, LCD_COLOR_GREEN);

    sprintf(app_lcd_print_buff, "Steps:");
    lcd_print_string_1608(APP_LCD_WELCOME_README_BASE_LINE+2,
                          app_lcd_print_buff, LCD_COLOR_GREEN);

    sprintf(app_lcd_print_buff, " - Press the screen");
    lcd_print_string_1608(APP_LCD_WELCOME_README_BASE_LINE+3,
                          app_lcd_print_buff, LCD_COLOR_GREEN);

    sprintf(app_lcd_print_buff, " - Read the passcode");
    lcd_print_string_1608(APP_LCD_WELCOME_README_BASE_LINE+4,
                          app_lcd_print_buff, LCD_COLOR_GREEN);

    sprintf(app_lcd_print_buff, "Speed \"one\" to start ...");
    lcd_print_string_1608(APP_LCD_WELCOME_README_BASE_LINE+6,
                          app_lcd_print_buff, LCD_COLOR_GREEN);
}

#if 0
void app_lcd_new_passcode(uint32_t pwd)
{
    lcd_clear_screen(LCD_COLOR_BLACK);

    sprintf(app_lcd_print_buff, "%d %d %d %d",
        (pwd / 1000 % 10), (pwd / 100 % 10), (pwd / 10 % 10), (pwd % 10)
        );
    lcd_print_string_3216_offset(1u, 3u, app_lcd_print_buff, LCD_COLOR_YELLOW);
}
#endif

void app_lcd_new_passcode(uint32_t *pwds)
{
    lcd_clear_screen(LCD_COLOR_BLACK);

    sprintf(app_lcd_print_buff, "%d %d %d %d", pwds[0], pwds[1], pwds[2], pwds[3] );
    lcd_print_string_3216_offset(1u, 3u, app_lcd_print_buff, LCD_COLOR_GRAY);
}

void app_lcd_show_passcode(uint32_t idx, uint32_t val)
{
    sprintf(app_lcd_print_buff, "%d", val);
    lcd_print_string_3216_offset(2u, 3u+idx*2, app_lcd_print_buff, LCD_COLOR_YELLOW);
}

void app_lcd_show_match(uint32_t condition)
{
    if (condition == 0)
    {
        sprintf(app_lcd_print_buff, " MATCH");
        lcd_print_string_3216_offset(5u, 3u, app_lcd_print_buff, LCD_COLOR_GREEN);
        sprintf(app_lcd_print_buff, "SUCCEED");
        lcd_print_string_3216_offset(6u, 3u, app_lcd_print_buff, LCD_COLOR_GREEN);
    }
    else
    {
        sprintf(app_lcd_print_buff, "MATCH");
        lcd_print_string_3216_offset(5u, 3u, app_lcd_print_buff, LCD_COLOR_RED);
        sprintf(app_lcd_print_buff, "FAILED");
        lcd_print_string_3216_offset(6u, 3u, app_lcd_print_buff, LCD_COLOR_RED);
    }
}

void app_lcd_show_nn_time(uint32_t us)
{
    sprintf(app_lcd_print_buff, "                ");
    lcd_print_string_1608(15u, app_lcd_print_buff, LCD_COLOR_YELLOW);

    if (us)
    {
        sprintf(app_lcd_print_buff, "nn_time: %d us", us);
        lcd_print_string_1608(15u, app_lcd_print_buff, LCD_COLOR_YELLOW);
    }
}

/* EOF. */

