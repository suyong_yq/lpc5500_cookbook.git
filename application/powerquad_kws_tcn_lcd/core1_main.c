/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdint.h>
#include <stdbool.h>

#include "fsl_common.h"
#include "shmem.h"

#include "lcd_hx8347d.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
void app_lcd_welcome(void);
void app_lcd_new_passcode(uint32_t *pwds);
void app_lcd_show_passcode(uint32_t idx, uint32_t pwd);
void app_lcd_show_match(uint32_t condition);
void app_lcd_show_nn_time(uint32_t us);

/*******************************************************************************
 * Variables
 ******************************************************************************/
uint32_t app_fifo1_data_raw;
shmem_cmd_msg_t *app_fifo1_cmd_msg_ptr;
uint32_t app_fifo1_param[4];
uint32_t app_fifo1_cmd;

/*******************************************************************************
 * Code
 ******************************************************************************/
/*
void my_delay(uint32_t t)
{
    for (uint32_t i = 0u; i < t; i++)
    {
        for (uint32_t j = 0u; j < 100000; j++)
        {
            asm("nop");
        }
    }
}
*/



/*!
 * @brief Main function
 */
int main(void)
{
    shmem_set_core_events(1u, SHMEM_CORE1_STAT_MAIN_START_MASK); /* boot ready. */

    lcd_init();
    shmem_set_core_events(1u, SHMEM_CORE1_STAT_HW_INIT_DONE_MASK); /* hardware ready. */

    shmem_fifo_init(1u);
    shmem_set_core_events(1u, SHMEM_CORE1_STAT_FIFO1_READY_MASK); /* fifo ready. */

    app_lcd_welcome();

    while (1)
    {
        app_fifo1_data_raw = shmem_fifo_pop(1u);
        if (app_fifo1_data_raw == 0)
        {
            continue;
        }

        /* read the message. */
        app_fifo1_cmd_msg_ptr = (shmem_cmd_msg_t *)app_fifo1_data_raw;
        app_fifo1_cmd = app_fifo1_cmd_msg_ptr->cmd;
        for (uint32_t i = 0u; i < 4u; i++)
        {
            app_fifo1_param[i] = app_fifo1_cmd_msg_ptr->param[i];
        }
        /* free the message . */
        app_fifo1_cmd_msg_ptr->is_full = false;

        switch (app_fifo1_cmd_msg_ptr->cmd)
        {
        case shmem_fifo1_cmd_lcd_new_passcode:
            app_lcd_new_passcode(app_fifo1_param);
            break;
        case shmem_fifo1_cmd_lcd_show_passcode:
            app_lcd_show_passcode(app_fifo1_param[0], app_fifo1_param[1]);
            break;
        case shmem_fifo1_cmd_lcd_show_match:
            app_lcd_show_match(app_fifo1_param[0]);
            break;
        case shmem_fifo1_cmd_lcd_show_nn_time:
            app_lcd_show_nn_time(app_fifo1_param[0]);
            break;
        default:
            break;
        }

        //led_on(1u<< 0u);
        //my_delay(10);
        //led_off(1u << 0u);
        //my_delay(10);
    }
}
/* EOF. */

