/* pq_tcn.h */

#ifndef __PQ_TCN_H__
#define __PQ_TCN_H__

#include <stdint.h>
#include <stdbool.h>
#include "arm_math.h"

void     pq_init_hardware(void);
void     pq_tcn_preprocess(uint16_t * audio_data);
void     pq_tcn_init(void);
uint32_t pq_tcn_run(void);

#endif /* __PQ_TCN_H__ */
