/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "app_inc.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define CONF_JSON_BUFF_LEN 1024

/*******************************************************************************
 * Variables
 ******************************************************************************/
FRESULT app_fs_err;
char conf_jsonstring[CONF_JSON_BUFF_LEN];

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
bool app_load_conf_json(char * filepath, char *jsonstring);
uint32_t app_get_default_index_from_conf(char *jsonstring);

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
    BOARD_InitPins();
    BOARD_BootClockPLL150M();
    BOARD_InitDebugConsole();

    printf("\r\ncjson basic.\r\n");

    /* setup the file system. */
    app_fs_err = fs_init();
    if (app_fs_err != FR_OK)
    {
        printf("fs_init() failed.\r\n");
        while (1);
    }
    printf("fs_init() done.\r\n");

    if ( !app_load_conf_json("/conf.json", conf_jsonstring) )
    {
        printf("app_load_conf_json() error.\r\n");
    }

    printf("root[default][index] = %d\r\n", app_get_default_index_from_conf(conf_jsonstring));

    printf("done.\r\n");

    while (1)
    {
    }
}


/* read the file's context into string. */
const char conf_json_filepath[] = "/conf.json";
bool app_load_conf_json(char * filepath, char *jsonstring)
{
    FRESULT err;
    FIL   ff_file;        /* File object */
    uint32_t ff_br;

    /* open file. */
    err = f_open(&ff_file, filepath, FA_READ);
    if (err != FR_OK)
    {
        f_close(&ff_file);
        //return err;
    }

#if 0
    /* print the text. */
    while (1)
    {
        f_read(&ff_file, ff_buff, FF_BUFF_LEN, &ff_br);
        for (uint32_t i = 0u; i < ff_br; i++)
        {
            putchar(ff_buff[i]);
        }
        if (ff_br != FF_BUFF_LEN)
        {
            break;
        }
    }
#endif

    f_read(&ff_file, jsonstring, CONF_JSON_BUFF_LEN, &ff_br);

     f_close(&ff_file);

     return  (ff_br < CONF_JSON_BUFF_LEN);
}

/* read the items from json. */
uint32_t app_get_default_index_from_conf(char *jsonstring)
{
    uint32_t ret = 0xff;
    cJSON *cjson_root = cJSON_Parse(jsonstring);

    /* get the root node. */
    if (cjson_root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            printf("Error before: %s\n", error_ptr);
        }
        cJSON_Delete(cjson_root);
        return ret;
    }

    /* read the default firmware's index. */
    cJSON * default_key = cJSON_GetObjectItemCaseSensitive(cjson_root, "default");
    cJSON * index_key = cJSON_GetObjectItemCaseSensitive(default_key, "idx");
    ret = (int)index_key->valueint;

    /* read firmware list. */
    cJSON *firmwares = cJSON_GetObjectItemCaseSensitive(cjson_root, "firmwares");
    cJSON *firmware = NULL;

    cJSON_ArrayForEach(firmware, firmwares)
    {
        cJSON *path = cJSON_GetObjectItemCaseSensitive(firmware, "path");
        cJSON *idx = cJSON_GetObjectItemCaseSensitive(firmware, "idx");

        printf("%2d:", idx->valueint );
        printf("%s\r\n", path->valuestring );

    }

  cJSON_Delete(cjson_root);

    return ret;
}



/* EOF. */

