/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "app_inc.h"

#include "fsl_sd_disk.h"
#include "diskio.h"
#include "ff.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static status_t sdcardWaitCardInsert(void);

/*******************************************************************************
 * Variables
 ******************************************************************************/
const TCHAR driverNumberBuffer[3U] = {SDDISK + '0', ':', '/'};
static FATFS g_fileSystem; /* File system object */
//static FIL g_fileObject;   /* File object */

/*! @brief SDMMC host detect card configuration */
static const sdmmchost_detect_card_t s_sdCardDetect =
{
#ifndef BOARD_SD_DETECT_TYPE
    .cdType = kSDMMCHOST_DetectCardByGpioCD,
#else
    .cdType = BOARD_SD_DETECT_TYPE,
#endif
    .cdTimeOut_ms = (~0U),
};

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */

FRESULT fs_init(void)
{
    FRESULT err;

    /* attach main clock to SDIF */
    CLOCK_AttachClk(kMAIN_CLK_to_SDIO_CLK);

    /* need call this function to clear the halt bit in clock divider register */
    CLOCK_SetClkDiv(kCLOCK_DivSdioClk, 6u, true); /* div = 5u. */

    if (sdcardWaitCardInsert() != kStatus_Success)
    {
        return FR_DISK_ERR;
    }

    err = f_mount(&g_fileSystem, driverNumberBuffer, 0U);
    if (FR_OK != err)
    {
        //PRINTF("Mount volume failed.\r\n");
        return err;
    }

#if (FF_FS_RPATH >= 2U)
    err = f_chdrive((char const *)&driverNumberBuffer[0U]);
    if (FR_OK != err)
    {
        //PRINTF("Change drive failed.\r\n");
        return err;
    }
#endif

    return err;
}

static status_t sdcardWaitCardInsert(void)
{
    /* Save host information. */
    g_sd.host.base           = SD_HOST_BASEADDR;
    g_sd.host.sourceClock_Hz = SD_HOST_CLK_FREQ;
    /* card detect type */
    g_sd.usrParam.cd = &s_sdCardDetect;

    /* SD host init function */
    if (SD_HostInit(&g_sd) != kStatus_Success)
    {
        //PRINTF("\r\nSD host init fail\r\n");
        return kStatus_Fail;
    }
    /* power off card */
    SD_PowerOffCard(g_sd.host.base, g_sd.usrParam.pwr);
    /* wait card insert */
    if (SD_WaitCardDetectStatus(SD_HOST_BASEADDR, &s_sdCardDetect, true) == kStatus_Success)
    {
        //PRINTF("\r\nCard inserted.\r\n");
        /* power on the card */
        SD_PowerOnCard(g_sd.host.base, g_sd.usrParam.pwr);
    }
    else
    {
        //PRINTF("\r\nCard detect fail.\r\n");
        return kStatus_Fail;
    }

    return kStatus_Success;
}


/* EOF. */

