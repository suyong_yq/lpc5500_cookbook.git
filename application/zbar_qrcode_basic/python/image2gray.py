from PIL import Image
# import matplotlib.pyplot as plt
import numpy as np

im = Image.open("image_mini.jpg")
print (im)

im_gray = im.convert('L')
print (im_gray)

im_gray_arr = np.array(im_gray)
print(len(im_gray_arr))
print(im_gray_arr)

with open('qrcode_image_data.c', 'w') as f:

    f.write("#include <stdint.h>")
    f.write('\n\n')

    f.write("/* [%d][%d] */" % (len(im_gray_arr), len(im_gray_arr[0])))
    f.write('\n')

    f.write('const uint8_t qrcode_image_data[] = {')
    f.write('\n')

    for line in im_gray_arr:
        for pixel in line:
            f.write("0x%-2x, " % (pixel))
        # f.write(",".join(str(i) for i in line) + ",")
        f.write('\n')

    f.write('};')
    f.write('\n')

