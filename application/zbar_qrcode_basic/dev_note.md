# qrcode_zbar_basic开发笔记

## zbar简介

项目主页: http://zbar.sourceforge.net/

zbar是日本的软件工程师开发的能扫描多种条形码和二维码的软件包, 开源,  可以运行在嵌入式平台上.

QR二维码的标准也是日本人提出的.

zbar处理QR二维码比较合适. 类似的产品还有谷歌开发的zxing, 也是开源的.

## 运行程序

1. 生成二维码

https://www.the-qrcode-generator.com/
可以通过这个网站将字符串转成二维码. 把图片保存到本地, 命名为image.jpg(或者image_mini.jpg以减少数据量)

2. 将二维码转成C数组

Python图像处理（一）【灰度化、二值化、灰度变换】
https://blog.csdn.net/lzwarhang/article/details/93209166

- 首先将图片文件通过python脚本(image2gray.py)转成c数组, 将数组直接编到工程中. 省略掉摄像头部分的代码.
- python脚本直接将图片数据变成灰度图, 对应zbar中的"Y800"数据格式. 
- 二值化的算法需要动态阈值, zbar可以搞定, 因此不需要硬件二值化, 让zbar在内部处理就好.

## 代码解析

1. 为了编译通过

本例中使用的zbar来源于原生的zbar, 并微调了同mcu程序不兼容的部分. 原生的代码是用gcc编译器编译的. 

在iar的编译器下, 使用的c99编译器并特别开启了定义可变长度数组(VLR)选项(options->C/C++ Compiler->Language 1->C dialect->Standard C->Allow VLA).

2. 增加堆和栈空间

在mcu上运行zbar要开足够大的stack和heap. 尤其是heap空间, 在编译过程中是不能确定其大小的, 所以即使不够, 编译器也不会报错. 但是在程序运行时会出现莫名其妙的hardfault. 本例中, 在linker file中, 为stack预留了0x2000(8K) Byte, 为heap预留了0x1f000(124K) Byte, 可以正常运行. 按说这个空间跟能够扫描到的图像大小有关. 本例中使用的是138x138的图片. zbar使用了malloc管理内存, 比较吃heap. 不过zbar本也是为mpu平台设计的, 实际的运行平台全部都是内存, heap空间充足, 只有mcu平台对内存空间的使用要多加小心.

3. 设置zbar的输入数据格式

~~~~~c
    zbar_image_set_format(image, *(int*)"Y800");
~~~~~

这里"Y800"指的是YUV像素格式中, Y占8位, UV都是0位, 也就是纯灰度图.
通过阅读代码发现, zbar_image_set_format()函数还能接收"GREY". 但其实还是灰度图.
也就是说, zbar接收的是8位灰度像素流. 如果以后外接摄像头, 也要先将摄像头的彩色数据转成灰度像素流.

4. 指定释放保存图像数据内存的函数

~~~~~c
    //zbar_image_set_data(image, qrcode_image_data, width * height, zbar_image_free_data);
    zbar_image_set_data(image, qrcode_image_data, width * height, NULL);
~~~~~

在调用zbar_image_set_data()函数为image赋值时, 除了指定图片的数据(qrcode_image_data)和大小(width * height)之外, 还指定了如何释放图片所占用的内存.
原生的样例工程中, 使用了预定义的zbar_image_free_data()函数释放保存图片的内存, 内部调用了free()函数. 但是在我的样例工程中, 这块内存是静态分配, 哪怕以后接入摄像头, 也是用DMA刷新的一块静态的内存, 不是malloc出来的内存, 不能free. 
我在移植过程中曾经使用原生的zbar_image_free_data()函数, 结果在程序运行到zbar_image_destroy(image)时会意外地exit(). 这里我根据实际情况, 传入NULL指针, 在需要释放image对象的时候, 不要对image->data, 即保存图像数据的内存, 进行任何处理. 终于妥了.



