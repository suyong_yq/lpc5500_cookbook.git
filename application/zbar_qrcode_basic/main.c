/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>

#include "fsl_common.h"
#include "board.h"
#include "pin_mux.h"

#include "zbar.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
extern const uint8_t qrcode_image_data[]; /* [138][138]. */

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
    uint8_t ch;

    board_init();
    printf("qrcode_zbar_basic.\r\n");

    /* create a reader. */
    zbar_image_scanner_t *scanner = zbar_image_scanner_create();
    zbar_image_scanner_set_config(scanner, 0, ZBAR_CFG_ENABLE, 1);

    /* create a image container. */
    zbar_image_t *image = zbar_image_create();
    zbar_image_set_format(image, *(int*)"Y800");
    uint32_t width = 138, height = 138;
    zbar_image_set_size(image, width, height);
    //zbar_image_set_data(image, qrcode_image_data, width * height, zbar_image_free_data);
    zbar_image_set_data(image, qrcode_image_data, width * height, NULL);

    /* scan the image for barcodes */
    int n = zbar_scan_image(scanner, image);

    /* extract results */
    for ( const zbar_symbol_t *symbol = zbar_image_first_symbol(image); symbol != NULL ; symbol = zbar_symbol_next(symbol) )
    {
        /* do something useful with results */
        zbar_symbol_type_t typ = zbar_symbol_get_type(symbol);
        const char *data = zbar_symbol_get_data(symbol);
        printf("decoded %s symbol \"%s\"\r\n", zbar_get_symbol_name(typ), data);
    }

    /* clean up */
    zbar_image_destroy(image);
    zbar_image_scanner_destroy(scanner);

    printf("done.\r\n");

    while (1)
    {
    	ch = getchar();
    	putchar(ch);
    }
}

/* EOF. */

