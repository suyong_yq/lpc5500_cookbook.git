/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>

#include "fsl_common.h"

#include "board.h"
#include "led.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/
void my_delay(uint32_t t)
{
    for (uint32_t i = 0u; i < t; i++)
    {
        for (uint32_t j = 0u; j < 100000; j++)
        {
            asm("nop");
        }
    }
}

/*!
 * @brief Main function.
 */
int main(void)
{
    board_init();
    printf("lpc5500 bootloader ram application example, ");
    printf("build at %s, on %s\r\n", __TIME__, __DATE__);

    led_init();

    while (1)
    {
        /* led on. */
        led_on(1u << 0u);
        my_delay(100);

        /* led off. */
        led_off(1u << 0u);
        my_delay(100);
    }
}

/* EOF. */

