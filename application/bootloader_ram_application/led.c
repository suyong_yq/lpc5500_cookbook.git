/* led.c */
#include "led.h"
#include "fsl_iocon.h"
#include "fsl_gpio.h"

uint8_t led_pin_map[][2] =
{
    { 1, 6},  /* red, PIO1_6 */
    { 1, 7}, /* green, PIO1_7 */
    { 1, 4}, /* blue, PIO1_4 */
};

void led_init(void)
{
    /* clock. */
    CLOCK_EnableClock(kCLOCK_Iocon);
    CLOCK_EnableClock(kCLOCK_Gpio1);

    /* iocon. */
    uint32_t iocon_pin_config = IOCON_PIO_FUNC(0)
                              | IOCON_PIO_MODE(0)
                              | IOCON_PIO_DIGIMODE(1)
                              | IOCON_PIO_FILTEROFF(1)
                              | IOCON_PIO_SLEW(0)
                              | IOCON_PIO_OD(0)
                              ;
    for (uint32_t i = 0u; i < LED_NUM; i++)
    {
        IOCON_PinMuxSet(IOCON, led_pin_map[i][0], led_pin_map[i][1], iocon_pin_config);
    }

    /* gpio. */
    gpio_pin_config_t gpio_pin_config;
    gpio_pin_config.pinDirection = kGPIO_DigitalOutput;
    gpio_pin_config.outputLogic  = 1u;
    for (uint32_t i = 0u; i < LED_NUM; i++)
    {
        GPIO_PinInit(GPIO, led_pin_map[i][0], led_pin_map[i][1], &gpio_pin_config);
    }
}

void led_on(uint32_t mask)
{
    for (uint32_t i = 0u; i < LED_NUM; i++)
    {
        if (0u != (mask & (1u << i)) )
        {
            GPIO_PinWrite(GPIO, led_pin_map[i][0], led_pin_map[i][1], 0u);
        }
    }
}

void led_off(uint32_t mask)
{
    for (uint32_t i = 0u; i < LED_NUM; i++)
    {
        if (0u != (mask & (1u << i)) )
        {
            GPIO_PinWrite(GPIO, led_pin_map[i][0], led_pin_map[i][1], 1u);
        }
    }
}

/* EOF. */

