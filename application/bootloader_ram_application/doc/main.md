﻿# Running application binary in SRAM with bootloader on LPC5500 MCU

Andrew SU (yong.su@nxp.com), 2020-03

[TOC]

## Introduction

In traditional MCU usage, the application executable image is downloaded into FLASH inside the chip, and run the code in FLASH directly. Per the hardware limitation, the speed of accessing to FLASH is much slower than the CPU's clock. Even the CACHE technology can improve the performance in some case, but it would also involve additional problems, like data synchronous problem. Developers would like to move some code to SRAM for better running performance, since the speed of accessing to SRAM is close to the CPU's clock and much faster than FLASH. However, the code in SRAM would be lost when the power is off and reset again, while only the FLASH can keep the code even the power is off.

As there are already some way to debug the code in SRAM within the IDE, but these ways would can only control the MCU when the power is always on. Once the POR (Power-On-Reset) occurs, the application loaded by external debugger would be lost. The way described in this paper would make a little improvement and resolves the issue that the code in SRAM can not be kept after the POR. In this paper, it shows a way of using customized bootloader to move the application image from FLASH to SRAM in booting period, and run it in SRAM, so that it can keep the image in FLASH but run in SRAM per every POR.

## Principle

Actually, the idea of using the customized bootloader is from dual core usage on LPC5500 MCU. when running the dual core application, the image for CPU1 (the slave core) should be built firstly. then it would be involved into the CPU0 (the master core)'s project during the compile. in CPU0's application, when it is like to activate the CPU1, it would move the original RAW image data for CPU1 from preserved FLASH area to SRAMX, feed the base address of the image into the "CPU1 boot address register", and finally enable the clock and release the CPU1 to run.

Even in the single core usage, the CPU0 can also be switched to the new running routine by setting a new base address to PC register in its own ARM Cortex-M33 core. Also, the stack pointer registers (PSP and MSP) should be updated, and the vector table should be remapped as well if the new application would run in a pure clean environment.

In the demo for this paper, the whole project would be divided into two standalone parts:

- application project. this project is opened to application developer. the coding for application project is just as the previous work running on FLASH, it can be even built standalone and used to create an executable binary file. the only difference is to use a customized linker file, to relocate the whole image to SRAM address space since they would be there when running. the application code in this period is using the so-called "run-time address".

- bootloader project. this project is playing the most important role now. it would be the default application running after the chip is power on. then it would copy the RAW data of application project's binary image manually to the indicated SRAM area. finally, do the magical things (setup PC, PSP, MSP and VTOR registers in the ARM core) to activate the CPU0 with the new environment for application image. there is a trick about downloading the application image into chip. even it can be downloaded through various kinds of ways, in the demo, with the easiest way, I just use the flashloader integrated in IDE (IAR) for bootloader project, as the application image can be downloaded into the chip along within the bootloader project. whatever, the application image should be downloaded into the chip, the application code in this period is using the so-called "download-time address".

The boot process from MCU Reset to application code would be in two steps, as in Figure 1.

![](./image/figure_mcu_boot_two_steps.png)

Figure 1 Two steps from MCU reset to application code

Another important thing is to arrange the memory area for each parts. there would be three part should be considered:

- application image in run-time
- application image in download-time
- bootloader image

The application image in download-time and the bootloader's code would be co-existing in the FLASH. the application image in run-time and the bootloader's RAM data would be co-existing in the SRAM. in the demo project, the memory areas are allocated as in the Table 1.

Table 1 Memory allocation for the demo project

| Address                          | Usage                               | Comment   |
| -------------------------------- | ----------------------------------- | --------- |
| 0x2000_0000 - 0x2002_FFFF, 192KB | application code in run-time        | SRAM0/1/2 |
| 0x2003_0000 - 0x2003_FFFF, 64KB  | application data in run-time        | SRAM3     |
| 0x0000_0000 - 0x0000_FFFF, 64KB  | bootloader code                     | FLASH     |
| 0x2003_0000 - 0x2003_FFFF, 64KB  | bootloader data                     | SRAM3     |
| 0x0001_0000 - 0x0003_FFFF, 192KB | application binary in download-time | FLASH     |

In the Table 1, it can be seen that the size of reserved memory space for application binary in download-time is the same  as the application code in run-time, as the binary in download-time would be copied to SRAM for run-time. The data space for application in run-time shares the same SRAM with the bootloader, as no conflict. Once the application is running, the bootloader would no longer run.

## Workflow

This section would show the detail description about creating the whole demo project (including application project and the bootloader project) step by step. In the demo project, it  lights on and off the LED on LPCXpresso55s69 EVK board. When the project is normally working, the red LED on the board would be on and off.

### Create the application project would be run in SRAM

The application project can be any user project with no code change specially for usage of bootloader. In the demo for this paper, a project is created from MCUXpresso SDK with a few modification for simplification, controlling the on-board red LED on and off. The only one and the most critical change is to update the linker file according to the Table 1.

In the "LPC55S69_cm33_application_ram.icf" file for application project, there are following code:

```javascript
define symbol m_interrupts_start               = 0x20000000; /* start from sram0. */
define symbol m_interrupts_end                 = 0x2000013F;

define symbol m_text_start                     = 0x2000013F;
define symbol m_text_end                       = 0x2002FFFF; /* end within sram2. */

define symbol m_data_start                     = 0x20030000; /* start from sram3. */
define symbol m_data_end                       = 0x2003FFFF; /* end within sram3. */
```

Also, please make sure the executable ".bin" file would be created after the building, as it could be executed directly after the copy work in bootloader project without any extraction. The Figure 2 shows this setting in IAR IDE.

![](./image/figure_create_bin_file_in_iar.png)

Figure 2 Create binary file in application project

### Create the bootloader project to copy the application's binary

The bootloader project is specially created to copy the application's binary after the POR. It could just simply do the copy work, with no operation to the peripheral, even do not change the clock system, just using the default clock after the POR. However, it integrated the application project's binary so that it can be downloaded into the FLASH inside the MCU through the IDE's flashloader. Of course, the application project's binary can be downloaded through the other way, for example the ISP or blhost.exe (the NXP's  bootloader client running on PC to communicate with the on-chip ROM). But using the IDE's flashloader is still the most simplest way for developer, just as they download the project during the debugging period.

1. Linker file

First of all, update the linker file according to the Table 1 for the bootloader project. There are codes in the "LPC55S69_cm33_bootloader_flash.icf" file:

```javascript
define symbol m_interrupts_start               = 0x00000000;
define symbol m_interrupts_end                 = 0x0000013F;

define symbol m_text_start                     = 0x00000140;
define symbol m_text_end                       = 0x0000FFFF; /* end with first 64KB. */

define exported symbol application_image_start = 0x00010000; /* for application image. 192KB. */
define exported symbol application_image_end   = 0x0003FFFF;

define symbol m_data_start                     = 0x20030000;
define symbol m_data_end                       = 0x2003FFFF; /* sram3. */

/* for user application binary. */
define region APPLICATION_region            = mem:[from application_image_start to application_image_end];
define block SEC_APPLICATION_IMAGE_BLOCK          { section  __sec_application };
place  in APPLICATION_region                      { block SEC_APPLICATION_IMAGE_BLOCK };

```

The first part defines the memory areas for bootloader project, and the application project's binary in download-time. The last part defines a memory block for the binary in bootloader project, so that it can be included into the bootloader project's binary (all in one) during the building. The section name "\__sec_application" quoted here would be defined in the project's option dialog.

Please keep in mind that  the application project's binary should still be visible to bootloader project, since the bootloader project need to recognize it and move it from FLASH to SRAM.

2. Project option

The work of including the pre-build application's binary into the bootloader project should be done in the project options dialog, shown as in Figure 3.

![](./image/figure_link_the_prebuild_binary_into_iar_project.png)

Figure 3 Link the pre-build binary file into bootloader project

In the "Raw binary image" box:

- File: "\$PROJ_DIR$\application_debug\application.bin"
- Symbol: "\_lpc5500_cm33_application_image"
- Section: "\__sec_application"
- Alignment: "4"

The "Symbol" would be used in the current project's source code, the "Section" is defined here and would be quoted in current project's linker file.

3. Source code

In the SystemInit() function, which is called before the main() function in the boot routine, it enables all the RAM bank to make sure the SRAM0/1/2/3 would be available for later work.

```c
void SystemInit(void)
{
    /* enable RAM banks that may be off by default at reset */
    SYSCON->AHBCLKCTRLSET[0] = SYSCON_AHBCLKCTRL0_SRAM_CTRL1_MASK
                             | SYSCON_AHBCLKCTRL0_SRAM_CTRL2_MASK
                             | SYSCON_AHBCLKCTRL0_SRAM_CTRL3_MASK
                             | SYSCON_AHBCLKCTRL0_SRAM_CTRL4_MASK;
}
```

Then in the main() function, which is the entry of user application in a project, it copies the binary from FLASH memory to SRAM memory and jump to run the new binary.

```c
#define APPLICATION_BOOT_ADDRESS (void *)0x20000000
extern uint8_t application_image_start[]; /* defined in linker file. */

int main(void)
{
    /* copy application image to sram0/1/2. */
#pragma section = "__sec_application" /* this section is defined in project option dialog. */
    uint32_t application_image_size = (uint32_t)__section_end("__sec_application") - (uint32_t)&application_image_start;

    /* copy the code for application from FLASH to the target memory. */
    memcpy(APPLICATION_BOOT_ADDRESS, (void *)application_image_start, application_image_size);

    /* jump to the application image. */
    JumpToImage(APPLICATION_BOOT_ADDRESS);
}
```

Note here the "APPLICATION_BOOT_ADDRESS" is defined as "0x2000_0000" to align with the linker file's setting in application project.

The "JumpToImage()" function is acting the most magic role. It resets the running environment for application code (vector table, stacks and the PC pointer), once the PC pointer is changed, the CPU would run to the new routine.

~~~c
typedef void(*func_0_t)(void); /* define the type of function with no parameter. */

void JumpToImage(void * addr)
{
    uint32_t * vectorTable = (uint32_t *)addr;
    uint32_t sp_base = vectorTable[0]; /* initial stack pointer. */
    func_0_t pc_func = (func_0_t)(vectorTable[1]); /* reset handler. */

    /* set new msp and psp. */
    __set_MSP(sp_base);
    __set_PSP(sp_base);

    /* remap the vector table. */
    SCB->VTOR = addr;

    /* jump to application. */
    pc_func();

    /* the code should never reach here. */
    while (1)
    {}
}
~~~

### Download and debug the application binary

To run the demo:

- Build the application project firstly, and generate the binary file "application.bin".
- Build the bootloader project, to involve the the pre-build "application.bin", and generate the all-in-one binary file "bootloader.bin". (actually the format of generated executable file is not cared, since it would be downloaded using the tools integrated in the IDE).
- Download the final all-in-one binary file in the bootloader project, using the download tool in IDE.
- Terminate the debug window, reset the board by pressing the on-board reset button, and run the demo.
- The red LED light on and off under the control of the application project. This means the demo is just running as expected.

For the first download, the above steps should be followed one by one, to make sure the bootloader and the application part are both available running on the chip. Once the bootloader part is already downloaded, then for later debug, developer can load the RAM binary directly in application project. Since the stack pointer registers (MSP, PSP) and the vector table register (VTOR) are already setup by the bootloader, when loading the run-time application binary directly in application project, the debugger would write the binary directly to SRAM memory and start the application in debug window. However, if the developer wants to make the final application project run after the POR with no IDE and debugger's help, just need to follow the above steps again, to keep the application binary is stored in the FLASH memory that would not lost when the power is off.

## Benchmark

The EEMBC coremark test is used to show the performance improvement of running code in SRAM. In the test project, the coremark thread is used as the application part, while the bootloader part is still the same as in the simple demo project. The LPC55S69 MCU is running with 150MHz core clock, with IAR compiler for ARM. The iterator times is set to 800, so that we would not wait for a long time (more than 10 seconds) for each test case.

| ![](./image/figure_coremark_flash_o3.png) | ![](./image/figure_coremark_ram_o3.png) |
| ----------------------------------------- | --------------------------------------- |
| FLASH code                                | RAM code                                |

Figure 4 EEMBC coremark records for FLASH code and RAM code

Finally, all the records in different optimization levels are summarized into Table 2.

Table 2 Coremark records on various compiler optimizations

| Optimization     | FLASH code | RAM code |
| ---------------- | ---------- | -------- |
| -o0 (none)       | 112.39     | 191.25   |
| -o1 (low)        | 131.39     | 213.62   |
| -o2 (medium)     | 182.23     | 280.11   |
| -o3 (high speed) | 265.16     | 405.06   |

![](./image/figure_coremark_records.png)

Figure 5 Coremark records on various compiler optimizations

From the benchmark test, it can be seen that the image the image running in SRAM is much faster then in the FLASH.



- End