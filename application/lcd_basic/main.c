/*
 * Copyright  2017 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>

#include "fsl_common.h"
#include "board.h"
#include "pin_mux.h"

#include "lcd_hx8347d.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
void app_systick_init(void);

/*******************************************************************************
 * Variables
 ******************************************************************************/
volatile uint32_t app_lcd_fps = 0u;
volatile uint32_t app_systick_count;


/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
	//uint8_t ch;

    BOARD_InitPins();
    BOARD_BootClockPLL150M();
    BOARD_InitDebugConsole();

    printf("\r\nlcd_basic.\r\n");

    lcd_init();

    app_systick_init();

    while (1)
    {
        lcd_clear_screen(LCD_COLOR_BLUE);
        app_lcd_fps++;
        lcd_clear_screen(LCD_COLOR_RED);
        app_lcd_fps++;
        lcd_clear_screen(LCD_COLOR_GREEN);
        app_lcd_fps++;
    }
}

void app_systick_init(void)
{
    CLOCK_AttachClk(kSYSTICK_DIV0_to_SYSTICK0);

    SysTick_Config( CLOCK_GetFreq(kCLOCK_CoreSysClk) / 1000u);
}

void SysTick_Handler(void)
{
    app_systick_count++;
    if (app_systick_count >= 1000u)
    {
        printf("fps: %d\r\n", app_lcd_fps);
        app_lcd_fps = 0u;
        app_systick_count = 0u;
    }
}

/* EOF. */

