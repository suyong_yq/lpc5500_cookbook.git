/* timer.h */

#ifndef __TIMER_H__
#define __TIMER_H__

#include "fsl_common.h"

#define TIMER_TICKS_PER_US 150u /* 150mhz. */

#define timer_init() do {\
    SYSCON->SYSTICKCLKDIV0 = SYSCON_SYSTICKCLKDIV0_DIV(0);/* use core clock with div=1. */ \
} while (0)

/* Systick Start */
#define timer_start()   do\
{\
    SysTick->LOAD = 0xFFFFFF ; /* set reload register */\
    SysTick->VAL  = 0;         /* Clear Counter */      \
    SysTick->CTRL = 0x5;       /* Enable Counting*/     \
} while (0)

/* Systick Stop and retrieve CPU Clocks count */
#define timer_stop(value)   do\
{\
    SysTick->CTRL  =0;        /* Disable Counting */              \
    value = SysTick->VAL;     /* Load the SysTick Counter Value */\
    value = 0xFFFFFF - value; /* Capture Counts in CPU Cycles*/   \
} while (0)

#endif /* __TIMER_H__ */
