/* rbuf32.c */
#include "rbuf32.h"

void RBUF32_Init(RBUF32_Handler_T *handler, uint32_t *bufPtr, uint32_t size)
{
    handler->BufferPtr = bufPtr;
    handler->BufferSize = size;
    handler->ReadIdx = 0u;
    handler->WriteIdx = 0u;
    handler->Counter = 0u;
}

/* clean the fifo. */
void RBUF32_Reset(RBUF32_Handler_T *handler)
{
    handler->WriteIdx = 0u;
    handler->ReadIdx = 0u;
    handler->Counter = 0u;
}

//static uint32_t _RBUF32_NextIdx(uint32_t idx, uint32_t maxCnt)
//{
//    return ( ((idx+1U) == maxCnt) ? 0U : (idx+1U) );
//}

bool RBUF32_IsEmpty(RBUF32_Handler_T *handler)
{
    //return (handler->ReadIdx == handler->WriteIdx);
    return (handler->Counter == 0u);
}

bool RBUF32_IsFull(RBUF32_Handler_T *handler)
{
    //return (  _RBUF32_NextIdx(handler->WriteIdx, handler->BufferSize) == handler->ReadIdx);
    return (handler->Counter == handler->BufferSize);
}

void RBUF32_PutDataIn(RBUF32_Handler_T *handler, uint32_t dat32)
{
    *((handler->BufferPtr)+(handler->WriteIdx)) = dat32; /* 将数据保存在当前指针 */
    //handler->WriteIdx = _RBUF32_NextIdx(handler->WriteIdx, handler->BufferSize);
    handler->WriteIdx = (handler->WriteIdx+1) % handler->BufferSize;
    handler->Counter++;
}

uint32_t RBUF32_GetDataOut(RBUF32_Handler_T *handler)
{
    uint32_t dat = *((handler->BufferPtr)+(handler->ReadIdx)); /* 从当前位置取数 */
    //handler->ReadIdx = _RBUF32_NextIdx(handler->ReadIdx, handler->BufferSize);
    handler->ReadIdx = (handler->ReadIdx+1) % handler->BufferSize;
    handler->Counter--;
    return dat;
}

/* EOF. */

