/*!
* @file    ring_buffer.h
* @brief   This component implements a ring buffer component based on a array of static memory.
* @version 1.0
* @author  Andrew SU (suyong_yq@126.com)
* @date    2015-09
*/

#ifndef __RBUF32_H__
#define __RBUF32_H__

#include <stdint.h>
#include <stdbool.h>

/*!
* @brief Define structure for Ring Buffer's handler.
*
* The Ring Buffer is origanized as FIFO based on a array of static memory.
*/
typedef struct
{
    uint32_t  BufferSize; /*!< Keep the available count of byte in buffer's array */
    uint32_t *BufferPtr;  /*!< Keep the first pointer to the buffer's array. This pointer is used to
                              handler the memory. */
    uint32_t ReadIdx;    /*!< Keep the index of item in static array for FIFO's read pointer. */
    uint32_t WriteIdx;   /*!< Keep the index of item in static array for FIFO's write pointer. */
    uint32_t Counter;    /*!< Record the current count of used item in the FIFO      .*/
} RBUF32_Handler_T;

/*!
* @brief Initialize the ring buffer.
*
* This function fills the allocated static memory into ring buffer's handle structure, and fills the
* read and write pointers with initial value.
*
* @param [out] handler Pointer to a empty ring buffer handle to be filled. See to #RBUF32_Handler_T.
* @param [in]  bufPtr  Pointer to allocated static memory.
* @param [in]  size    Count of memory size in byte.
*/
void RBUF32_Init(RBUF32_Handler_T *handler, uint32_t *bufPtr, uint32_t size);

/*!
* @brief Reset or Clean the ring buffer.
*
* This function clear all the items in the fifo. Actually, it just resets the write index, read
* index and the item count, then all the old data would be unavailable, while the new data would
* cover them.
*
* @param [in] handler Pointer to a ring buffer handle to be clean. See to #RBUF32_Handler_T.
*/
void RBUF32_Reset(RBUF32_Handler_T *handler);

/*!
* @brief Check if the buffer is empty.
*
* @param [in] handler Pointer to an available handler to be operated.
* @retval true  The buffer is empty.
* @retval false The buffer is not empty.
*/
bool RBUF32_IsEmpty(RBUF32_Handler_T *handler);

/*!
* @brief Check if the buffer is full.
*
* @param [in] handler Pointer to an available handler to be operated.
* @retval true  The buffer is full.
* @retval false The buffer is not full.
*/
bool RBUF32_IsFull(RBUF32_Handler_T *handler);

/*!
* @brief Put data into buffer.
*
* @param [in] handler Pointer to an available handler to be operated.
* @param [in] dat The data to be put into buffer.
*/
void RBUF32_PutDataIn(RBUF32_Handler_T *handler, uint32_t dat32);

/*!
* @brief Get out data from the buffer.
*
* @param [in] handler Pointer to an available handler to be operated.
* @retval The data from the buffer.
*/
uint32_t RBUF32_GetDataOut(RBUF32_Handler_T *handler);

#endif /* __RING_BUFFER_H__ */
