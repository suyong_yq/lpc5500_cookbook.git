

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _USE_FONTS_H
#define _USE_FONTS_H


/* Includes ------------------------------------------------------------------*/
//#include "MacroAndConst.h"

#include "stdint.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern const uint8_t c_chFont1206[95][12];
extern const uint8_t c_chFont1608[95][16];

extern char g_lcdPrintBuffer[];

void lcd_print_string_1608(uint16_t line_idx, char *buf, uint16_t hwColor);
void lcd_print_string_1206(uint16_t line_idx, char *buf, uint16_t hwColor);
void lcd_print_string_3216(uint16_t line_idx, char *buf, uint16_t hwColor);
void lcd_print_string_3216_offset(uint16_t line_idx, uint16_t item_offset, char *buf, uint16_t hwColor);
#endif

/*-------------------------------END OF FILE-------------------------------*/

