/**
  ******************************************************************************
  * @file    xxx.c
  * @author  Waveshare Team
  * @version
  * @date    xx-xx-2014
  * @brief   xxxxx.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, WAVESHARE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
//#include "LIB_Config.h"
#include "lcd_fonts.h"
#include "lcd_hx8347d.h"

void lcd_display_char_3216(uint16_t hwXpos, //specify x position.
                           uint16_t hwYpos, //specify y position.
                           uint8_t  chChr,   //a char to display.
                           uint16_t hwColor) //specify the color of the char
{
	uint8_t i, j, chTemp;
	uint16_t hwYpos0 = hwYpos, hwColorVal = 0;

	if (hwXpos >= LCD_WIDTH || hwYpos >= LCD_HEIGHT) {
		return;
	}


    for (i = 0; i < LCD_FONT_1608; i ++)
    {
        chTemp = c_chFont1608[chChr - 0x20][i]; /* 使用1608的字模作为基础, 一个点扩展为四个点 */

        for (j = 0; j < 8; j ++)
        {
    		if (chTemp & 0x80)
            {
				hwColorVal = hwColor;
				lcd_draw_point(hwXpos  , hwYpos  , hwColorVal);
                lcd_draw_point(hwXpos+1, hwYpos  , hwColorVal);
                lcd_draw_point(hwXpos  , hwYpos+1, hwColorVal);
                lcd_draw_point(hwXpos+1, hwYpos+1, hwColorVal);
    		}
			chTemp <<= 1;
			hwYpos ++; hwYpos ++;
			if ((hwYpos - hwYpos0) == LCD_FONT_1608*2) /* 3216 */
            {
				hwYpos = hwYpos0;
				hwXpos ++; hwXpos ++;
				break;
			}
		}
    }
}

//char g_lcdPrintBuffer[32];

void lcd_print_string_1608(uint16_t line_idx, char *buf, uint16_t hwColor)
{
    uint16_t y = line_idx * 16u;
    uint16_t x = 0u;

    while (x < (LCD_WIDTH-16u) )
    {
        lcd_display_char(x, y, *buf++, LCD_FONT_1608, hwColor);
        if (*buf == '\0')
        {
            break;
        }
        else if ( (*buf == '\r') || (*buf == '\n') )
        {
            break;
        }
        else
        {
            x += 8u;
        }
    }
}

void lcd_print_string_1206(uint16_t line_idx, char *buf, uint16_t hwColor)
{
    uint16_t y = line_idx * 12u;
    uint16_t x = 0u;

    while (x < (LCD_WIDTH-12u) )
    {
        lcd_display_char(x, y, *buf++, LCD_FONT_1608, hwColor);
        if (*buf == '\0')
        {
            break;
        }
        else if ( (*buf == '\r') || (*buf == '\n') )
        {
            break;
        }
        else
        {
            x += 6u;
        }
    }
}

void lcd_print_string_3216(uint16_t line_idx, char *buf, uint16_t hwColor)
{
    uint16_t y = line_idx * 32u;
    uint16_t x = 0u;

    while (x < (LCD_WIDTH-32u) )
    {
        lcd_display_char_3216(x, y, *buf++, hwColor);
        if (*buf == '\0')
        {
            break;
        }
        else if ( (*buf == '\r') || (*buf == '\n') )
        {
            break;
        }
        else
        {
            x += 16u;
        }
    }
}

void lcd_print_string_3216_offset(uint16_t line_idx, uint16_t item_offset, char *buf, uint16_t hwColor)
{
    uint16_t y = line_idx * 32u;
    uint16_t x = item_offset * 16 /* 0u */;

    while (x < (LCD_WIDTH-32u) )
    {
        lcd_display_char_3216(x, y, *buf++, hwColor);
        if (*buf == '\0')
        {
            break;
        }
        else if ( (*buf == '\r') || (*buf == '\n') )
        {
            break;
        }
        else
        {
            x += 16u;
        }
    }
}

/*-------------------------------END OF FILE-------------------------------*/

